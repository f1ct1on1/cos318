/* kernel.c */

#include "common.h"
#include "kernel.h"
#include "scheduler.h"
#include "th.h"
#include "util.h"

#include "tasks.c"

pcb_t *current_running;

static pcb_t pcbArray[NUM_TASKS];
static pcb_node_t nodeArray[NUM_TASKS];

/* This function is the entry point for the kernel
 * It must be the first function in the file
 */
void _start(void)
{
    /* Set up the single entry-point for system calls */
    *ENTRY_POINT = &kernel_entry;

    clear_screen(0, 0, 80, 25);

    /* Initialize the pcbs and the ready queue */
    for (int i = 0; i < NUM_TASKS; i++) {
    	pcb_t *pcb = &pcbArray[i];
    	pcb->p_id = i;
    	pcb->flags = 0;
    	for (int j = 0; j < 8; j++) {
    		pcb->registers[j] = 0;
    	}

        pcb->user_stack = STACK_MIN+(2*i+1)*STACK_SIZE;
        pcb->kernel_stack = STACK_MIN+(2*i+2)*STACK_SIZE;
        pcb->eip = task[i]->entry_point;

        pcb->user_stack -= 4;
        *(uint32_t *)pcb->user_stack = pcb->eip;
        pcb->kernel_stack -= 4;
        *(uint32_t *)pcb->kernel_stack = pcb->eip;

    	pcb->state = READY_STATE;
        pcb->task_type = task[i]->task_type;
        pcb_node_t *node = &nodeArray[i];
        node->pcb = pcb;
        pcb->node = node;
        enqueue(&ready_queue, &nodeArray[i]);

    }

    //print task_info
    //print_hex(1, 1, task[0]->entry_point);
    //print_hex(2, 1, pcbArray[0].eip);
    //print_hex(3, 1, *(uint32_t *)(pcbArray[0].esp));
    //delay(10000);

    // enqueue(&ready_queue, &nodeArray[0]);
    // enqueue(&ready_queue, &nodeArray[1]);
    // enqueue(&ready_queue, &nodeArray[2]);
    // enqueue(&ready_queue, &nodeArray[3]);
    // enqueue(&ready_queue, &nodeArray[4]);
    // //print_int(1, 1, pcbArray[0].task_type);
    // //print_int(2, 1, pcbArray[4].task_type);
    // //delay(10);
    // enqueue(&ready_queue, &nodeArray[5]);
    /* Schedule the first task */
    scheduler_count = 0;
    //asm volatile("xchg %ebx, %ebx");

    scheduler_entry();
    /* We shouldn't ever get here */
    ASSERT(0);
}

