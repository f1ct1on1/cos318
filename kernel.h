/* kernel.h: definitions used by kernel code */

#ifndef KERNEL_H
#define KERNEL_H

#include "common.h"

/* ENTRY_POINT points to a location that holds a pointer to kernel_entry */
#define ENTRY_POINT ((void (**)(int)) 0x0f00)

/* System call numbers */
enum {
    SYSCALL_YIELD,
    SYSCALL_EXIT,
};

enum {
	READY_STATE,
	RUNNING_STATE,
	BLOCKED_STATE,
	EXITED_STATE
};

/* All stacks should be STACK_SIZE bytes large
 * The first stack should be placed at location STACK_MIN
 * Only memory below STACK_MAX should be used for stacks
 */
enum {
    STACK_MIN = 0x40000,
    STACK_SIZE = 0x1000,
    STACK_MAX = 0x52000,
};

struct pcb;

/* Queue struct definitions */
typedef struct pcb_node {
	struct pcb *pcb;
	struct pcb_node *next;
} pcb_node_t;

typedef struct pcb_queue {
	pcb_node_t *head;
	pcb_node_t *tail;
} pcb_queue_t;

typedef struct pcb {
	uint32_t flags;
	uint32_t registers[8];
	uint32_t user_stack;
	uint32_t kernel_stack;
	int task_type;
	int eip;
	int state;
	int p_id;	
	pcb_node_t *node;


} pcb_t;


/* The task currently running */
extern pcb_t *current_running;

void kernel_entry(int fn);

#endif                          /* KERNEL_H */
