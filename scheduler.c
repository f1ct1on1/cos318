/* scheduler.c */

#include "common.h"
#include "kernel.h"
#include "scheduler.h"
#include "util.h"

int scheduler_count;
int enqueue_count = 0;
pcb_queue_t ready_queue = {NULL, NULL};
pcb_queue_t blocked_queue = {NULL, NULL};

void enqueue(pcb_queue_t *queue, pcb_node_t *node) {
	if (queue->head == NULL) {
		queue->head = node;
		queue->tail = node;
		queue->head->next = NULL;
		return;
	}

	queue->tail->next = node;
	queue->tail = node;
}

pcb_node_t *dequeue(pcb_queue_t *queue) {
	if (is_empty(queue)) return NULL;
	pcb_node_t *node = queue->head;
	queue->head = node->next;
	return node;
}

int is_empty(pcb_queue_t *queue) {
	if (queue->head == NULL) return 1;
	return 0;
}

void scheduler(void)
{
    ++scheduler_count;
    pcb_node_t *node = dequeue(&ready_queue);
    current_running = node->pcb;
    while(!current_running) {
    	print_str(24, 50, "Nothing running");
    	do_yield();
    }
    //print_int(1, 1, current_running->task_type);
    //delay(5);
    //asm volatile ("xchg %bx, %bx");
}

void do_yield(void)
{
	// reschedule the current task
	enqueue(&ready_queue, current_running->node);

	// save context and switch to the next task
	scheduler_entry();
}

void do_exit(void)
{
	current_running->state = EXITED_STATE;
	//enqueue(&ready_queue, current_running->node);
	scheduler_entry();
}

void block(void)
{
	current_running->state = BLOCKED_STATE;
	enqueue(&blocked_queue, current_running->node);
	scheduler_entry();
}

void unblock(void)
{
	enqueue(&ready_queue, dequeue(&blocked_queue));
}

bool_t blocked_tasks(void)
{
	return FALSE;
}
